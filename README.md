Function: start_profile
	Argument: String which defines the time we are measuring
	Description: Initializes the time measurement
	Returns: void

Function: end_profile
	Argument: String which defines the time we are measuring
	Description: Finishes the time measurement and prints it
	Returns: unsigned long representing the quantity of microseconds since you called start_profile 


Note:
Obviously, it adds overhead
