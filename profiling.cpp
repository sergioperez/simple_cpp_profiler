#include "profiling.h"
//TODO:
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <map>

using namespace std::chrono;
using namespace std;

std::map<string, system_clock::time_point> times;

void start_profile(std::string str)
{
	system_clock::time_point start = system_clock::now();
	times[str] = start;
}

unsigned long end_profile(std::string str)
{
	system_clock::time_point end = system_clock::now();
	system_clock::time_point start = times[str];
	auto duration = std::chrono::duration_cast<nanoseconds>(end - start);
	auto duration_num = duration.count();
	cout << "#MYPROFILER:\t" << "Function=" << str << "\tTime=" << duration_num << " nanoseconds" << endl;
	return ((unsigned long) duration_num);
}

/*void fun_lenta()
{
	start_profile("fun_lenta");
	sleep(1);
	end_profile("fun_lenta");
}

void fun_rapida()
{
	start_profile("fun_rapida");
	end_profile("fun_rapida");
}*/

/*int main()
{
	start_profile("main");
	fun_lenta();
	fun_rapida();
	sleep(1);
	unsigned long time = end_profile("main");
}*/
